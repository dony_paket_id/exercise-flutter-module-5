import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';

const String _url = 'https://flutter.dev';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<XFile>? _imageFileList;
  String? dony;
  final ImagePicker _picker = ImagePicker();
  bool isVideo = false;
  VideoPlayerController? _videoController;

  void _launchURL() async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  Future<void> _playVideo(XFile? file) async {
    if (file != null) {
      await _disposeVideoController();
      late VideoPlayerController videoController;
      videoController = VideoPlayerController.file(File(file.path));
      _videoController = videoController;
      await _videoController!.initialize();
      await _videoController!.setLooping(true);
      await _videoController!.play();
      setState(() {});
    }
  }

  void _getImageAndVideoResource(
    ImageSource source, {
    required BuildContext? context,
    bool isMultipleImages = false,
  }) async {
    try {
      if (isVideo) {
        if (source == ImageSource.camera) {
          final pickedFile = await _picker.pickVideo(
            source: source,
            maxDuration: const Duration(seconds: 10),
          );
          await _playVideo(pickedFile);
        } else {
          final pickedFile = await _picker.pickVideo(source: source);
          await _playVideo(pickedFile);
        }
      } else if (isMultipleImages) {
        _disposeVideoController();

        final pickedFile = await _picker.pickMultiImage();

        setState(() {
          _imageFileList = pickedFile;
        });
      } else {
        _disposeVideoController();

        final pickedFile = await _picker.pickImage(source: source);

        setState(() {
          _imageFileList = pickedFile == null ? null : [pickedFile];
        });
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Widget _previewImage() {
    try {
      if (_imageFileList != null) {
        return ListView.builder(
          itemBuilder: (context, index) {
            return Image.file(File(_imageFileList![index].path));
          },
          itemCount: _imageFileList!.length,
        );
      } else {
        return const Text("You need to pick an image");
      }
    } catch (e) {
      return Text(e.toString());
    }
  }

  Widget _previewVideo() {
    try {
      if (_videoController == null) {
        return const Text("Pick a video");
      } else {
        return AspectRatio(
          child: VideoPlayer(_videoController!),
          aspectRatio: _videoController!.value.aspectRatio,
        );
      }
    } catch (e) {
      return Text(e.toString());
    }
  }

  @override
  void dispose() {
    _disposeVideoController();
    super.dispose();
  }

  Future<void> _disposeVideoController() async {
    if (_videoController != null) {
      await _videoController!.dispose();
    }

    _videoController = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppBar(
          title: const Text("Package"),
        ),
      ),
      body: Center(child: isVideo ? _previewVideo() : _previewImage()),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              isVideo = false;
              _getImageAndVideoResource(ImageSource.gallery, context: context);
            },
            child: const Icon(Icons.image_search),
          ),
          const SizedBox(height: 20),
          FloatingActionButton(
            onPressed: () {
              isVideo = false;
              _getImageAndVideoResource(ImageSource.camera, context: context);
            },
            child: const Icon(Icons.camera),
          ),
          const SizedBox(height: 20),
          FloatingActionButton(
            onPressed: () {
              isVideo = false;
              _getImageAndVideoResource(ImageSource.gallery,
                  context: context, isMultipleImages: true);
            },
            child: const Icon(Icons.photo_library),
          ),
          const SizedBox(height: 20),
          FloatingActionButton(
            onPressed: () {
              isVideo = true;
              _getImageAndVideoResource(ImageSource.gallery, context: context);
            },
            child: const Icon(Icons.video_library),
          ),
          const SizedBox(height: 20),
          FloatingActionButton(
            onPressed: () {
              isVideo = true;
              _getImageAndVideoResource(ImageSource.camera, context: context);
            },
            child: const Icon(Icons.video_camera_back),
          ),
          const SizedBox(height: 20),
          FloatingActionButton(
            onPressed: _launchURL,
            child: const Icon(Icons.network_locked_sharp),
          )
        ],
      ),
    );
  }
}

class TestDispose extends StatefulWidget {
  TestDispose({Key? key}) : super(key: key);

  @override
  State<TestDispose> createState() => _TestDisposeState();
}

class _TestDisposeState extends State<TestDispose> {
  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.yellow);
  }

  @override
  void dispose() {
    super.dispose();
  }
}
